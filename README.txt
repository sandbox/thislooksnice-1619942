Description
-----------
This module extends the Webform module and enables users to load Webform fields with data from their most recent submission.

Requirements
------------
Drupal 6.x

Installation
------------
1. Copy the entire webform_repeat directory the Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Site
   Building" -> "Modules"

3. Edit the settings under "Administer" -> "Site configuration" ->
   "Webform Repeat" to enable Webforms for which this functionality should be available

Support
-------
Please use the issue queue for filing bugs with this module (sandbox) at
http://drupal.org/project/issues/1619942
