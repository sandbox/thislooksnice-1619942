<?php

/**
 * @file
 * Administration pages provided by Webform Repeat module.
 */

/**
 * Menu callback for admin/settings/webform_repeat.
 */
function webform_repeat_admin_settings() {
  module_load_include('inc', 'webform', 'includes/webform.admin');
    
  $webform_types = webform_variable_get('webform_node_types');
  print_r(node_get_types('module', 'node_type'));
  // Load all webforms on the site
  $nodes = array();
  if ($webform_types) {
    $placeholders = implode(', ', array_fill(0, count($webform_types), "'%s'"));
    $result = db_query(db_rewrite_sql("SELECT n.*, r.* FROM {node} n INNER JOIN {node_revisions} r ON n.vid = r.vid WHERE n.type IN ($placeholders)", 'n', 'nid', $webform_types), $webform_types);
    while ($node = db_fetch_object($result)) {
      $webform_nodes[$node->nid] = $node->title;
    }
  }
  
  if(!empty($webform_nodes)) {
    
    $enabled_webform_nodes = webform_repeat_get_enabled_webforms();
    // Set up the form:
    $form['enabled_webform_nodes'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Enabled Webforms'),
      '#description' => t('These are the webforms available for Webform Repeat. You may enabled any of these webforms by checking its corresponding box. Only enabled webforms will offer the user the ability to load data from previous submissions.'),
      '#options' => $webform_nodes,
      '#default_value' => $enabled_webform_nodes
    );
  }
  
  // If there are no webforms, display a message
  else {
    if (empty($webform_types)) {
      $message = t('Webform is currently not enabled on any content types.') . ' ' . t('Visit the <a href="!url">Webform settings</a> page and enable Webform on at least one content type.', array('!url' => url('admin/settings/webform')));
    }
    else {
      $webform_type_list = webform_admin_type_list();
      $message = t('There are currently no webforms on your site. Create a !types piece of content.', array('!types' => $webform_type_list));
    }

    $form['webforms'] = array(
      '#type' => 'fieldset',
      '#description' => t($message),
      '#title' => t('No Available Webforms'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
  }

  
  $form = system_settings_form($form);
  array_unshift($form['#submit'], 'webform_repeat_admin_settings_submit');

  return $form;
}

/**
 * Submit handler for the webform_admin_settings() form.
 */
function webform_repeat_admin_settings_submit($form, &$form_state) {
  $enabled_webform_nodes = array();
  foreach ($form_state['values']['enabled_webform_nodes'] as $key => $nid) {
    if ($nid) {
      $enabled_webform_nodes[] = $nid;
    }
  }
  $enabled_webform_nodes = implode(",", $enabled_webform_nodes);
  
  // Save system variable and we're done!
  variable_set("webform_repeat_enabled_webforms", $enabled_webform_nodes);
}